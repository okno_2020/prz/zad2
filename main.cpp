using namespace std;
#include <iostream>

int main() {
    const int D = 11;
    int noOfDivisible = 0;
    int smallest = INT_MAX;
    int noOfNegativePairs = 0;
    int lastNumber = 0;
    int currentNumber;

    do {
        cout << "podaj jaks liczbe" << endl;
        cin >> currentNumber;

        smallest = min(currentNumber, smallest);
        if (currentNumber % D == 0) {
            noOfDivisible++;
        }
        if (currentNumber < 0 && lastNumber < 0) {
            noOfNegativePairs++;
        }
        lastNumber = currentNumber;

    } while (!(currentNumber % 3 == 0 || currentNumber % 2 == 0));

    cout << "najmniejsza wczytana liczba to: " + to_string(smallest) << endl;
    cout << "ilosc liczb podzielnych przez " + to_string(D) + " to: " + to_string(noOfDivisible) << endl;
    cout << "ilosc wczytanych kolejno po sobie ujemnych par to: " + to_string(noOfNegativePairs) << endl;
    return 0;
}
