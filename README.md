# Zadanie 2 - z pętlą do-while (0. 5 pkt)

Napisać program, który wczytuje ciąg liczb całkowitych aż do wczytania liczby parzystej lub
podzielnej przez 3. Wydrukować następujące informacje (biorąc pod uwagę wszystkie
wczytane liczby):

- najmniejszą wczytaną liczbę
- ile było w tym ciągu liczb podzielnych przez stałą D
- ile razy wczytano kolejno, po sobie, dwie liczby ujemne.

UWAGA: Program napisać bez użycia tablic, z wykorzystaniem pętli do-while.
Wskazówka: Definiowanie stałych za pomocą const jest omówione na końcu lekcji 1.1 (wartość
stałej możemy wpisać dowolną), podzielność liczb całkowitych – w lekcji 2.1.
